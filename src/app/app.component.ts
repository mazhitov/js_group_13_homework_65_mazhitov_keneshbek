import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { HttpService } from '../shared/http.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy {
  movieName = '';
  deleteId = '';
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;
  moviesUploadingSubscription!: Subscription;
  moviesDestroyingSubscription!: Subscription;

  movies: Movie[] = [];

  isLoading = false;
  isUploading = false;
  isDestroying = false;

  constructor(private httpService: HttpService){}

  ngOnInit() {
    this.httpService.getMovies();
    this.moviesChangeSubscription = this.httpService.moviesChange.subscribe((movies:Movie[]) => {
      this.movies = movies;
    });
    this.moviesFetchingSubscription = this.httpService.moviesFetching.subscribe((isFetching:boolean) => {
      this.isLoading = isFetching;
    });
    this.moviesUploadingSubscription = this.httpService.movieUploading.subscribe((isUploading:boolean) => {
      this.isUploading = isUploading;
    });
    this.moviesDestroyingSubscription = this.httpService.movieDestroying.subscribe((isDestroying:boolean) => {
      this.isDestroying = isDestroying;
    });
    this.httpService.fetchMovie();
  }



  onSubmit() {
    const newMovie = new Movie('', this.movieName);
    this.httpService.addMovie(newMovie).subscribe(() => {
      this.httpService.fetchMovie();
    });
    this.movieName = '';
  }

  onDelete(movie: Movie) {
    this.deleteId = movie.id;
    this.httpService.deleteMovie(movie).subscribe(() => {
      this.httpService.fetchMovie();
    });
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
    this.moviesUploadingSubscription.unsubscribe();
    this.moviesDestroyingSubscription.unsubscribe();
  }
}
