import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from './movie.model';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class HttpService {
  private movies: Movie[] = [];
  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  movieUploading = new Subject<boolean>();
  movieDestroying = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getMovies() {
    return this.movies.slice();
  }


  fetchMovie() {
    this.moviesFetching.next(true);
    this.http.get<{ [id: string]: Movie }>('https://project-server-788da-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        if (!result) {
          return [];
        }
        return Object.keys(result).map(id => {
          const movie = result[id]
          return new Movie(id, movie.name);
        });
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      }, error => {
        this.moviesFetching.next(false);
        console.log(new Error(error.message));
      });
  }

  addMovie(newMovie: Movie) {
    const postBody = {
      name: newMovie.name
    };
    this.movieUploading.next(true);
    return this.http.post('https://project-server-788da-default-rtdb.firebaseio.com/movies.json', postBody).pipe(
      tap(() => {
        this.movieUploading.next(false);
      }, () => {
        this.movieUploading.next(false);
      })
    );
  }

  deleteMovie(movie: Movie) {
    this.movieDestroying.next(true);
    return this.http.delete('https://project-server-788da-default-rtdb.firebaseio.com/movies/' + movie.id + '.json')
      .pipe(
        tap(() => {
          this.movieDestroying.next(false);
        }, () => {
          this.movieDestroying.next(false);
        })
      );
  }
}
